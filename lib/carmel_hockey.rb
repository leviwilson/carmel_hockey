# frozen_string_literal: true

require 'active_model'
require 'memoist'
require 'nokogiri'
require 'open-uri'

require 'hockey_model'
require 'carmel_hockey/leader'
require 'carmel_hockey/player'
require 'carmel_hockey/season'
require 'carmel_hockey/team'
require 'carmel_hockey/version'

module CarmelHockey
  class Error < StandardError; end

  def self.seasons
    Season.all
  end
end
