# frozen_string_literal: true

module CarmelHockey
  class Season
    include HockeyModel

    attr_accessor :id, :name

    def initialize(attributes = {})
      super
      self.id = id.to_i
    end

    def leaders
      Leader.all(season: id)
    end
    memoize :leaders

    class << self
      extend Memoist
      def all
        page.css('select[name="sel_ChildSeason"] option').map do |option|
          Season.new(id: option.attribute('value').text, name: option.text)
        end
      end
      memoize :all

      private

      def page
        Nokogiri::HTML(open('http://admin.esportsdesk.com/leagues/stats_hockey.cfm?leagueID=16561&clientID=4754'))
      end
    end
  end
end
