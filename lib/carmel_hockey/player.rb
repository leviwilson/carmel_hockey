# frozen_string_literal: true

module CarmelHockey
  class Player
    include HockeyModel

    attr_accessor :id, :name
  end
end
