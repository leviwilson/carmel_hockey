# frozen_string_literal: true

module CarmelHockey
  class Team
    include HockeyModel

    attr_accessor :id, :name
  end
end
