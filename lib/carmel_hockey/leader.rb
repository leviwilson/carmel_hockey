# frozen_string_literal: true

module CarmelHockey
  class Leader
    include HockeyModel

    COLUMNS = %i[
      rank _name number position _team
      games_played goals assists
      points points_per_game penalty_minutes
    ].freeze

    attr_accessor :player, :team
    attr_accessor(*COLUMNS.reject { |c| c.to_s.start_with?('_') })

    def initialize(attributes = {})
      super
      self.points_per_game = points_per_game.to_f
      %i[goals assists games_played rank points penalty_minutes number].each do |attr|
        send("#{attr}=", send(attr).to_i)
      end
    end

    class << self
      extend Memoist

      def all(season:)
        seasons[season] ||= leaders_for(season)
      end

      private

      def from_row(row)
        columns = row.css 'td'

        table_values = COLUMNS.each_with_index.map do |name, index|
          [name, columns[index].text.strip] unless name.to_s[0] == '_'
        end.compact

        Leader.new Hash[table_values.concat(models_for(columns))]
      end

      def models_for(columns)
        { _name: :player, _team: :team }.map do |id_name, name|
          column = columns[COLUMNS.find_index(id_name)]
          next unless column.css('a').first

          model_for name, column
        end.compact
      end

      def model_for(name, column)
        id = column.css('a').first['href'].match(/#{name}ID=(\d+)/)[1].to_i
        [name, "carmel_hockey/#{name}".classify.constantize.new(id: id, name: column.text.strip)]
      end

      def seasons
        {}
      end
      memoize :seasons

      def leaders_for(season)
        leaders = []
        page = 0

        loop do
          leaders << page(season: season, page: page += 1)
          break if leaders.last.count < 20
        end

        leaders.flatten
      end

      STATS = 'http://admin.esportsdesk.com/leagues/stats_hockey.cfm'

      def page(season:, page:)
        offset = (20 * (page - 1)) + 1
        url = "#{STATS}?leagueID=16561&clientID=4754&start_row=#{offset}"
        html = Nokogiri::HTML(Net::HTTP.post_form(URI.parse(url), sel_ChildSeason: season).body)
        html.css('tr.boxscores_tables5').map(&method(:from_row))
      end
    end
  end
end
