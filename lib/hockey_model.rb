# frozen_string_literal: true

module HockeyModel
  extend ActiveSupport::Concern

  included do
    include ActiveModel::Model
    extend Memoist
  end

  def attributes
    instance_variables.each_with_object({}) do |name, h|
      name = name[1..-1]
      h[name] = send(name)
    end
  end
end
