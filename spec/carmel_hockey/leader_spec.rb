# frozen_string_literal: true

module CarmelHockey
  RSpec.describe Leader do
    let(:season) { Season.all[1] }

    subject { season.leaders.first }

    its(:player) { should have_attributes id: 1_826_894, name: 'Raivis Grunte', class: CarmelHockey::Player }
    its(:team) { should have_attributes id: 534_226, name: "Rascal's", class: CarmelHockey::Team }
    its(:rank) { should eq 1 }
    its(:number) { 17 }
    its(:position) { should eq '-' }
    its(:games_played) { should eq 9 }
    its(:goals) { should eq 15 }
    its(:assists) { should eq 6 }
    its(:points) { should eq 21 }
    its(:points_per_game) { should eq 2.3 }
    its(:penalty_minutes) { should eq 3 }

    its(:attributes) do
      should match hash_including 'player' => be_instance_of(CarmelHockey::Player),
                                  'team' => be_instance_of(CarmelHockey::Team),
                                  'rank' => 1,
                                  'position' => '-',
                                  'games_played' => 9,
                                  'goals' => 15,
                                  'assists' => 6,
                                  'points' => 21,
                                  'points_per_game' => 2.3,
                                  'penalty_minutes' => 3,
                                  'number' => 17
    end

    context '.all' do
      subject { described_class.all(season: season) }

      its(:count) { should eq 106 }
      its('last.player.name') { should eq 'Kevin Thompson' }
    end
  end
end
