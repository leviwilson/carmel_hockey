# frozen_string_literal: true

module CarmelHockey
  RSpec.describe Season do
    let(:season) { described_class.new id: '12345', name: 'Ferda Season' }

    subject { season }

    its(:id) { should eq 12_345 }
    its(:name) { should eq 'Ferda Season' }

    its('attributes') { should eq 'id' => 12_345, 'name' => 'Ferda Season' }

    context '#leaders' do
      let(:leaders) { [instance_double(Leader)] }

      before { allow(Leader).to receive(:all).and_return leaders }

      subject do
        season.leaders
        Leader
      end

      it { should have_received(:all).with season: season.id }

      it 'only calls it once' do
        season.leaders
        should have_received(:all).once
      end
    end

    context '.all' do
      subject { described_class.all }

      its(:count) { should eq 29 }

      context 'attributes' do
        subject { super().find { |s| s.name == 'Summer 2019' } }

        its(:name) { should eq 'Summer 2019' }
        its(:id) { should eq 57_583 }
      end
    end
  end
end
