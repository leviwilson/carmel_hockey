# frozen_string_literal: true

RSpec.describe CarmelHockey do
  it 'has a version number' do
    expect(CarmelHockey::VERSION).not_to be nil
  end

  context '.seasons', :no_recording do
    subject { described_class.seasons }

    its(:count) { should be_positive }

    its('first.leaders.count') { should be_positive }
  end
end
