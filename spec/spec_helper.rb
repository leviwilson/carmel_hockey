# frozen_string_literal: true

require 'carmel_hockey'
require 'rspec/its'
require 'vcr'

VCR.configure do |config|
  config.cassette_library_dir = 'spec/fixtures/vcr_cassettes'
  config.hook_into :webmock

  config.default_cassette_options = {
    record: :new_episodes
  }

  config.configure_rspec_metadata!

  config.around_http_request do |request|
    VCR.use_cassette('esports', &request)
  end
end

WebMock.allow_net_connect!

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.around(:example, :no_recording) do |example|
    [CarmelHockey::Season, CarmelHockey::Leader].each(&:unmemoize_all)

    VCR.turn_off! ignore_cassettes: true
    example.run
    VCR.turn_on!
  end

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
